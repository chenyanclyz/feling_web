#!/usr/bin/python
# coding:utf-8

# import unittest
import logging

from feling import db
import time
# logging.basicConfig(level=logging.DEBUG)
# '''simple test of db.py not fully'''
from config import configs
db.create_engine(**(configs.db))
# db.update('drop table if exists user')

# db.update('create table user (\
#             id int not null primary key auto_increment,\
#             name text not null,\
#             passwd text not null,\
#             email text,\
#             last_modified real not null)')
# user = db.Dict(name='chenyan', email='chenyanclyz@163.com',
#                passwd='123', last_modified=time.time())
# db.insert('user', **user)
# print db.update('select ?? from user where email=?', 'name','chenyanclyz@163.com')
# print db.select('select ?? from user where name = ?', "email", "chenyan")
# print db.select('select email from user where name = ?', "notexists")
# print db.select_one('select ?? from user where id = ?','*', 1)



'''init tables in the database'''
# db.update('drop table if exists users')
db.update('drop table if exists blogs')
# db.update('drop table if exists comments')

# table_users = '''
# create table users (
#     `id` varchar(50) not null,
#     `email` varchar(50) not null,
#     `password` varchar(50) not null,
#     `admin` bool not null,
#     `name` varchar(50) not null,
#     `image` varchar(500) not null,
#     `liveness` bigint not null,
#     `created_at` real not null,
#     unique key `idx_email` (`email`),
#     key `idx_created_at` (`created_at`),
#     primary key (`id`)
# ) engine=innodb default charset=utf8
# '''

table_blogs = '''
create table blogs (
    `id` varchar(50) not null,
    `user_id` varchar(50) not null,
    `user_name` varchar(50) not null,
    `user_image` varchar(500) not null,
    `title` varchar(50) not null,
    `summary` varchar(200) not null,
    `content` mediumtext not null,
    `level` varchar(1) not null,
    `zan` bigint not null,
    `belongs` varchar(2) not null,
    `created_at` real not null,
    key `idx_created_at` (`created_at`),
    primary key (`id`)
) engine=innodb default charset=utf8
'''

table_comments = '''
create table comments (
    `id` varchar(50) not null,
    `blog_id` varchar(50) not null,
    `user_id` varchar(50) not null,
    `user_name` varchar(50) not null,
    `user_image` varchar(500) not null,
    `content` mediumtext not null,
    `created_at` real not null,
    key `idx_created_at` (`created_at`),
    primary key (`id`)
) engine=innodb default charset=utf8
'''
# db.update(table_users)
db.update(table_blogs)
# db.update(table_comments)



# '''test of models.py'''
from models import User, Blog, Comment

# from feling import db

# db.create_engine(user='root', password='xiaoff', database='webapp_py')

# u = User(name='Tebbfbst', email='test@debbbbvbxeb.com', password='123456f7890', image='abofut:blank', aaa=1 )

# u.insert()

# print 'new user id:', u.id

# u1 = User.find_first('where email=?', 'test@example.com')
# print "find user's name:", u1.name

# # u1.delete()

# u2 = User.find_first('where email=?', 'test@example.com')
# print 'find user:', u2

